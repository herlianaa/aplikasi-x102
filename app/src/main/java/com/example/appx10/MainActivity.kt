package com.example.appx10

import android.app.ProgressDialog
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_signup.*

class MainActivity : AppCompatActivity(), View.OnClickListener{
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnLogin -> {
                var email: String = edUserName.text.toString()
                var password: String = edPassword.text.toString()

                if (email.isEmpty() || password.isEmpty()) {
                    Toast.makeText(this, "Username / password can't be empty", Toast.LENGTH_SHORT)
                        .show()
                } else {
                    val progressDialog = ProgressDialog(this)
                    progressDialog.isIndeterminate = true
                    progressDialog.setMessage("Authenticatting...")
                    progressDialog.show()

                    FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener {
                            progressDialog.hide()
                            if (!it.isSuccessful) return@addOnCompleteListener
                            Toast.makeText(this, "Sucessfully Login", Toast.LENGTH_SHORT).show()
                            val intent = Intent(this, SignInActivity::class.java)
                            startActivity(intent)

                        }
                        .addOnFailureListener {
                            progressDialog.hide()
                            Toast.makeText(
                                this,
                                "Username/password incorrect", Toast.LENGTH_SHORT
                            ).show()
                        }

                }
            }
            R.id.txSignUp -> {
                var intent = Intent(this, SignUpActivity::class.java)
                startActivity(intent)
            }

            }
        }



        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_main)
            btnLogin.setOnClickListener(this)
            txSignUp.setOnClickListener(this)

        }



}
